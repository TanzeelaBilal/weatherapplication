package com.project.weather.network;
import com.project.weather.model.response.ForecastResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
        @GET("forecast?")
        Observable<ForecastResponse> getForecastData(@Query("id") String id, @Query("appid") String app_id);

}
