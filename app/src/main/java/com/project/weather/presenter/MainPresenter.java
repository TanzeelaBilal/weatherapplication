package com.project.weather.presenter;

import android.content.Context;

import com.project.weather.R;
import com.project.weather.model.bean.List;
import com.project.weather.model.response.ForecastResponse;
import com.project.weather.network.ApiService;
import com.project.weather.network.RetrofitClient;
import com.project.weather.utils.Constants;
import com.project.weather.view.views.MainView;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter {
    java.util.List<List> weatherData;
    MainView view;
    Context context;

    public MainPresenter(MainView mainView, Context context) {
        this.view = mainView;
        this.context = context;
    }

    public void getData() {
        new RetrofitClient().getRetrofit().create(ApiService.class).getForecastData(Constants.ID, Constants.WEATHER_APP_ID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ForecastResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ForecastResponse forecastResponse) {
                        weatherData = forecastResponse.getList();
                        view.updateText(makeText());
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showToast();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private String makeText() {
        StringBuilder tvStringBuilder = new StringBuilder();
        tvStringBuilder.append(context.getResources().getString(R.string.weather_today)).append("\n");
        if (weatherData != null && weatherData.size() > 0) {
            tvStringBuilder.append(context.getResources().getString(R.string.temp));
            tvStringBuilder.append(weatherData.get(0).getMain().getTemp()).append("\n");
            tvStringBuilder.append(context.getResources().getString(R.string.temp_min));
            tvStringBuilder.append(weatherData.get(0).getMain().getTemp_min()).append("\n");
            tvStringBuilder.append(context.getResources().getString(R.string.temp_max));
            tvStringBuilder.append(weatherData.get(0).getMain().getTemp_max()).append("\n");
            tvStringBuilder.append(context.getResources().getString(R.string.pressure));
            tvStringBuilder.append(weatherData.get(0).getMain().getPressure());
        }
        return tvStringBuilder.toString();
    }

}
