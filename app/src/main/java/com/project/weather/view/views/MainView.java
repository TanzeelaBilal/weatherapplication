package com.project.weather.view.views;

public interface MainView {
    public void updateText(String text);
    public void showToast();
}
